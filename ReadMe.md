# Array Methods

## flat()

Parameters : It accepts depth parameter (number).

Returns : A new array with the sub-array elements concatenated into it.

Example : 

    const arr1 = [0, 1, 2, [3, 4]];

    console.log(arr1.flat());
    // expected output: [0, 1, 2, 3, 4]

    const arr2 = [0, 1, 2, [[[3, 4]]]];

    console.log(arr2.flat(3));
    // expected output: [0, 1, 2, 3, 4]

flat() means : It will access the sub array to specific depth and return with new array

Does it mutate the original array?

No, it does not mutate the original array

## concat()

Parameters : It accepts valueN parameter. n (any) number of values (number, string, boolean, array, null, undefined, object and function etc).

Returns : A new Array instance

Example :

    const array1 = ['a', 'b', 'c'];
    const array2 = ['d', 'e', 'f'];
    const array3 = array1.concat(array2);

    console.log(array3);
    // expected output: Array ["a", "b", "c", "d", "e", "f"]

concat() means : It is used to merge two or more arrays. It doesn't change the existing array.

Does it mutate the original array?

No, it does not mutate the original array

## push()

Parameters : (elementN) The element(s) to add to the end of the array.

Returns : The new length property of the object upon which the method was called.

Example : 
    const animals = ['pigs', 'goats', 'sheep'];

    const count = animals.push('cows');
    console.log(count);
    // expected output: 4
    console.log(animals);
    // expected output: Array ["pigs", "goats", "sheep", "cows"]


push() means: It adds values at the end of the array without disturbing the order in which they're passed.

Does it mutate the original array?

Yes, It mutates the original array.

## pop()

Parameters : It doesn't have any parameter.

Returns : It returns the last element which is popped out of the original array.

Example : 
    const plants = ['broccoli', 'cauliflower', 'cabbage', 'kale', 'tomato'];

    console.log(plants.pop());
    // expected output: "tomato"


pop() means: It pops out the last element from the given array.

Does it mutate the original array?

Yes, It mutates the original array.

## shift()

Parameters : It does not have any parameters.

Returns  : It returns the first element that has been removed from the array.

Example :

    let arr = [1,2,3,4]

    console.log(arr.shift()); // 1

shift() means : It removes the first element from the given array.

Does it mutate the original array?

Yes, It mutate the original array.

## unshift

Parameters : It accepts any number of parameters.

Returns : It will return the length of the updated array;

Example :

    let arr = [1,2,3];

    arr.unshift(4,5,6)

    console.log(arr.unshift(4,5,6)); // 6

    console.log(arr) = [4,5,6,1,2,3];

unshift() means : It updates the array by adding the given values at the beginning of the array.

Does it mutate the original array?

Yes it mutates the original array.

## indexOf

Parameters : It accepts two parameters one for search element and the other to specify the starting index to begin search.

Returns : It returns the first index of the search element in the given array if it is found otherwise it returns -1.

Example :

    let arr = [1,2,3];

    console.log(arr.indexOf(3)); // 2

indexOf() means : This method searches for the given element in the array and returns the index where it first encounters it.

Does it mutate the original array?

No, It does not mutate the original array.

## lastIndexOf

Parameters : It accepts two parameters one the search element and the second the starting index to begin search.

Returns : It will return the last index at which the given element could be found in the given array.

Example :

    let arr = [1,2,3,3];

    console.log(arr.lastIndexOf(3)); // 3

lastIndexOf() means : It just searches for the last index at which given element could be found in the array.

Does it mutate the original array?

No, It does not mutate the original array.

## includes

Parameters : It has two parameters one is the element to search for and the other is the index to begin our search with.

Returns : It returns either true or false.

Example :

    let arr =[1,2,3,4];

    console.log(arr.includes(3)); // true

includes() means : It searches the array for given value if the array contains that value it will return true;

Does it mutate the original array?

No, It does not mutate the original array.

## reverse

Parameters : It does not have any parameters.

Returns : It will reverses the entire array and returns it.

Example :

    let arr = [2,3,4];

    console.log(arr.reverse()); // [4,3,2];

reverse() means : It reverses the entire array in place.

Does it mutate the original array?

Yes, It mutates the original array.

## splice

Parameters : It has a start parameter and delete count parameter and the elements that we want to add at start index.

Returns : An array containing the deleted elements.

Example :

    let arr = [1,2,3,4,5];

    console.log(arr.splice(2,2,['ammulu','nani])); // [3,4]

splice() means : It can be used to add elements in middle of the array and it is also useful in deleting the elements from middle of the array. 

Does it mutate the original array?

Yes, It mutates the original array.

## slice

Parameters : It takes two parameters start and end.

Returns : It returns a new array which has all the elements extracted from the start to end.

Example :

    let arr = [2,3,4,5];

    console.log(arr.slice(1,4)); // [3,4,5]

slice() means : It slices the the given array at the start and at the end and returns that part as a new array.

Does it mutate the original array?

No, It does not mutate the original array.

## foreach

Parameters : It has only one parameter that takes the callback function.

Returns : It returns undefined.

Example :

    let arr = [1,2,3];

    function callback(x) {
        
        console.log(x);
    }

    console.log(arr.foreach(callback)); // 1 2 3

foreach() means : It iterates through the entire array passing each element to the function.

Does it mutate the original array?

No, It does not mutate the original array.

## map

Parameters : It takes one essential parameter which is callback function and an optional parameter where we can pass 'this'.

Returns : A new array where each element is obtained by passing the elements of the original array to the function.

Example :

    let arr =[1,2,3];

    console.log(arr.map((element)=> {2*element}))

map() means : It iterates through the array passes the elements to the callback function and puts the return value into a new array.

Does it mutate the original array?

No, It does not mutate the original array.

## filter

Parameters : It accepts a callback function as parameter.

Returns : A new array consisting of all the elements in the original array which returns true when supplied to our callback function.

Example : 

    let arr = [1,2,3];

    console.log(arr.filter((elem)=> { return elem > 2}); // [3]

filter() means : This method filters out the elements from the parent array which do not result in true when supplied to the callback function and only returns the other elements in a new array.

Does it mutate the original array?

No, It does not mutate the original array.

## find

Parameters: It accepts a callback function as a parameter.

Returns : The first element in the array which results in true when passed to the callback function. If no element returns true it return undefined.

Example :

    let arr = [1,2,3];

    console.log(arr.find((elem)=>((elem%2) == 1))); // 1

find() means : It finds the first element that returns true when passed to callback function and it returns that element.

Does it mutate the original array?

No, It does not mutate the original array.

## findIndex

Parameters : It accepts a callback function as a parameter.

Returns : It returns the index of the first element in the array which returns true when passed to the callback function. If no element returns true it returns -1.

Example : 

    let arr = [1,2,4,8];

    console.log(arr.findIndex((elem)=> {return (elem%2) == 0})); // 1 the index of 2

findIndex() means : It returns the index of the first element which satisfies our condition, if no element passes our condition it will return -1.

Does it mutate the original array?

No, It does not mutate the original array.

## some

Parameters : It accepts the callback function as its first parameter and this as an optional parameter.

Returns : It returns true if atleast one element passes the given condition otherwise it return false. A boolean value.

Example :

    let arr = [1,2,3];

    console.log(arr.some((elem)=>(elem%2 == 0))); // true

some() means : A method which return true even if one element passes the test if nothing passes then it would return false.

Does it mutate the original array?

No, It does not mutate the original array.

## every

Parameters : It needs a callback function as the main parameter and 'this' as an optional parameter.

Returns : It returns true if every element returns true when passed to the callback function otherwise false.

Example :

    let arr = [1,2,3];

    console.log(arr.every((elem)=>(elem>0))); // true

every() means : If every element in the array satisfies the condition we provided then this method returns true else it returns false.

Does it mutate the original array?

No,It does not mutate the original array.

## sort

Parameters : It takes an optional comparision function as a parameter.

Returns : It retuns the original array sorted in place.

Example :

    let arr = [3,2,0];

    console.log(arr.sort((a,b)=> a-b)); // [0,2,3]

sort() means : The default compare function is based upon the UTF -16 code and sorts them in an ascending order of that code. If we have numbers then we've to write compare function.

Does it mutate the original array?

Yes, It does mutate the original array.

## reduce

parameters : It takes two parameters one is callback function and the other is accumulator value.

returns : A value that results from reducer callback function.

Example :

    let a = [1,2,3,4];

    console.log(a.reduce((sum,elem) => sum+element,0)); // 10

reduce() means : It reduces the entire array to a single value.

Does it mutate the original array?

No, It does not mutate the original array.

